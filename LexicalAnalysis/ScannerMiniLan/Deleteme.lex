import java.lang.System;
class Scanner {
public static void main(String argv[])
        throws java.io.IOException {
Yylex yy = new Yylex(System.in);
while (yy.yylex() != null){}
}
}

class Yytoken {Yytoken () {}}

%%

CHAPTER = (Chapter|chapter)
SKETCH = (Sketch|sketch)
END = (End|end)
AD = (Ad|ad)
UPPERLETTER = ([A-Z])
LETTER=([A-Za-z])
ACTION = ("TO_"{UPPERLETTER}{UPPERLETTER}({LETTER}*))
NAME = ({UPPERLETTER}{LETTER}*)
ACTORS = ({NAME}|{NAME}" "{NAME})
LOWERCASE = ([a-z])
OBJECT =  ({LOWERCASE}*" "{LOWERCASE}*|{LOWERCASE}*)
%%

{CHAPTER} 		{System.out.println("SCANNER:: CHAPTER");}
{SKETCH}		{System.out.println("SCANNER:: SKETCH");}
{END}			{System.out.println("SCANNER:: END");}
{AD}			{System.out.println("SCANNER:: AD");}
{ACTION}		{System.out.println("SCANNER:: ACTION");}
,			{System.out.println("SCANNER:: COMMA");}
"("			{System.out.println("SCANNER:: OP");}
")" 			{System.out.println("SCANNER:: CP");}
{ACTORS}		{System.out.println("SCANNER:: ACTOR " + yytext());}
{OBJECT} 		{System.out.println("SCANNER:: OBJECT " +yytext());}
(" "|\n|\t)		{System.out.println("SCANNER:: SEPARATOR");}
. 	     		{System.out.println("SCANNER:: Unmatched input "+ yytext());}



