import java.lang.System;
class Scanner {
public static void main(String argv[])
        throws java.io.IOException {
Yylex yy = new Yylex(System.in);
while (yy.yylex() != null){}
}
}

class Yytoken {Yytoken () {}}

%%

DIGIT=([0-9])
INTEGER = ({DIGIT}+)
LETTER= ([a-zA-Z])
BLANK = (" "|\t)
COMMENT = ((//)({DIGIT}|{LETTER}|{BLANK})*))
REAL = ({INTEGER}"."|{INTEGER}"."{INTEGER}|"."{INTEGER})
EXT= ({LETTER}|{DIGIT})
IDENT = ({LETTER}{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?)


%%

{IDENT}			{System.out.println("SCANNER:: IDENT <"+ yytext() +">");}
;			{System.out.println("SCANNER:: EOS");}
"+"      	     	{System.out.println("SCANNER:: PLUS");}
begin		        {System.out.println("SCANNER:: BEGIN");}
{COMMENT}		{System.out.println("SCANNER:: COMMENT");}
"<="			{System.out.println("SCANNER:: LE");}
">="			{System.out.println("SCANNER:: GE");}
"<"			{System.out.println("SCANNER:: LT");}
">"			{System.out.println("SCANNER:: GT");}
"=="			{System.out.println("SCANNER:: EQ");}
"="			{System.out.println("SCANNER:: SET");}
"end"  			{System.out.println("SCANNER:: END");}
{REAL}			{System.out.println("SCANNER:: NUMBER <"+ yytext() +">");}
{DIGIT}			{System.out.println("SCANNER:: NUMBER <"+ yytext() +">");}
"print"			{System.out.println("SCANNER:: PRINT");}
-			{System.out.println("SCANNER:: MINUS");}
{INTEGER}    		{System.out.println("SCANNER:: NUMBER <"+ yytext() +">");}
(" "|\t|\n)+   		{}
"*"			{System.out.println("SCANNER:: MULT");}
/			{System.out.println("SCANNER:: DIV");}
"("			{System.out.println("SCANNER:: LP");}
")"			{System.out.println("SCANNER:: RP");}
. 	     		{System.out.println("SCANNER:: Unmatched input "+ yytext());}


