import java.lang.System;
import java_cup.runtime.Symbol;


class Yylex implements java_cup.runtime.Scanner {
	private final int YY_BUFFER_SIZE = 512;
	private final int YY_F = -1;
	private final int YY_NO_STATE = -1;
	private final int YY_NOT_ACCEPT = 0;
	private final int YY_START = 1;
	private final int YY_END = 2;
	private final int YY_NO_ANCHOR = 4;
	private final int YY_BOL = 128;
	private final int YY_EOF = 129;
	private java.io.BufferedReader yy_reader;
	private int yy_buffer_index;
	private int yy_buffer_read;
	private int yy_buffer_start;
	private int yy_buffer_end;
	private char yy_buffer[];
	private boolean yy_at_bol;
	private int yy_lexical_state;

	Yylex (java.io.Reader reader) {
		this ();
		if (null == reader) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(reader);
	}

	Yylex (java.io.InputStream instream) {
		this ();
		if (null == instream) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(new java.io.InputStreamReader(instream));
	}

	private Yylex () {
		yy_buffer = new char[YY_BUFFER_SIZE];
		yy_buffer_read = 0;
		yy_buffer_index = 0;
		yy_buffer_start = 0;
		yy_buffer_end = 0;
		yy_at_bol = true;
		yy_lexical_state = YYINITIAL;
	}

	private boolean yy_eof_done = false;
	private final int YYINITIAL = 0;
	private final int yy_state_dtrans[] = {
		0
	};
	private void yybegin (int state) {
		yy_lexical_state = state;
	}
	private int yy_advance ()
		throws java.io.IOException {
		int next_read;
		int i;
		int j;

		if (yy_buffer_index < yy_buffer_read) {
			return yy_buffer[yy_buffer_index++];
		}

		if (0 != yy_buffer_start) {
			i = yy_buffer_start;
			j = 0;
			while (i < yy_buffer_read) {
				yy_buffer[j] = yy_buffer[i];
				++i;
				++j;
			}
			yy_buffer_end = yy_buffer_end - yy_buffer_start;
			yy_buffer_start = 0;
			yy_buffer_read = j;
			yy_buffer_index = j;
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}

		while (yy_buffer_index >= yy_buffer_read) {
			if (yy_buffer_index >= yy_buffer.length) {
				yy_buffer = yy_double(yy_buffer);
			}
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}
		return yy_buffer[yy_buffer_index++];
	}
	private void yy_move_end () {
		if (yy_buffer_end > yy_buffer_start &&
		    '\n' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
		if (yy_buffer_end > yy_buffer_start &&
		    '\r' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
	}
	private boolean yy_last_was_cr=false;
	private void yy_mark_start () {
		yy_buffer_start = yy_buffer_index;
	}
	private void yy_mark_end () {
		yy_buffer_end = yy_buffer_index;
	}
	private void yy_to_mark () {
		yy_buffer_index = yy_buffer_end;
		yy_at_bol = (yy_buffer_end > yy_buffer_start) &&
		            ('\r' == yy_buffer[yy_buffer_end-1] ||
		             '\n' == yy_buffer[yy_buffer_end-1] ||
		             2028/*LS*/ == yy_buffer[yy_buffer_end-1] ||
		             2029/*PS*/ == yy_buffer[yy_buffer_end-1]);
	}
	private java.lang.String yytext () {
		return (new java.lang.String(yy_buffer,
			yy_buffer_start,
			yy_buffer_end - yy_buffer_start));
	}
	private int yylength () {
		return yy_buffer_end - yy_buffer_start;
	}
	private char[] yy_double (char buf[]) {
		int i;
		char newbuf[];
		newbuf = new char[2*buf.length];
		for (i = 0; i < buf.length; ++i) {
			newbuf[i] = buf[i];
		}
		return newbuf;
	}
	private final int YY_E_INTERNAL = 0;
	private final int YY_E_MATCH = 1;
	private java.lang.String yy_error_string[] = {
		"Error: Internal error.\n",
		"Error: Unmatched input.\n"
	};
	private void yy_error (int code,boolean fatal) {
		java.lang.System.out.print(yy_error_string[code]);
		java.lang.System.out.flush();
		if (fatal) {
			throw new Error("Fatal Error.\n");
		}
	}
	private int[][] unpackFromString(int size1, int size2, String st) {
		int colonIndex = -1;
		String lengthString;
		int sequenceLength = 0;
		int sequenceInteger = 0;

		int commaIndex;
		String workString;

		int res[][] = new int[size1][size2];
		for (int i= 0; i < size1; i++) {
			for (int j= 0; j < size2; j++) {
				if (sequenceLength != 0) {
					res[i][j] = sequenceInteger;
					sequenceLength--;
					continue;
				}
				commaIndex = st.indexOf(',');
				workString = (commaIndex==-1) ? st :
					st.substring(0, commaIndex);
				st = st.substring(commaIndex+1);
				colonIndex = workString.indexOf(':');
				if (colonIndex == -1) {
					res[i][j]=Integer.parseInt(workString);
					continue;
				}
				lengthString =
					workString.substring(colonIndex+1);
				sequenceLength=Integer.parseInt(lengthString);
				workString=workString.substring(0,colonIndex);
				sequenceInteger=Integer.parseInt(workString);
				res[i][j] = sequenceInteger;
				sequenceLength--;
			}
		}
		return res;
	}
	private int yy_acpt[] = {
		/* 0 */ YY_NOT_ACCEPT,
		/* 1 */ YY_NO_ANCHOR,
		/* 2 */ YY_NO_ANCHOR,
		/* 3 */ YY_NO_ANCHOR,
		/* 4 */ YY_NO_ANCHOR,
		/* 5 */ YY_NO_ANCHOR,
		/* 6 */ YY_NO_ANCHOR,
		/* 7 */ YY_NO_ANCHOR,
		/* 8 */ YY_NO_ANCHOR,
		/* 9 */ YY_NO_ANCHOR,
		/* 10 */ YY_NO_ANCHOR,
		/* 11 */ YY_NO_ANCHOR,
		/* 12 */ YY_NO_ANCHOR,
		/* 13 */ YY_NO_ANCHOR,
		/* 14 */ YY_NO_ANCHOR,
		/* 15 */ YY_NO_ANCHOR,
		/* 16 */ YY_NO_ANCHOR,
		/* 17 */ YY_NOT_ACCEPT,
		/* 18 */ YY_NO_ANCHOR,
		/* 19 */ YY_NO_ANCHOR,
		/* 20 */ YY_NO_ANCHOR,
		/* 21 */ YY_NOT_ACCEPT,
		/* 22 */ YY_NO_ANCHOR,
		/* 23 */ YY_NO_ANCHOR,
		/* 24 */ YY_NOT_ACCEPT,
		/* 25 */ YY_NO_ANCHOR,
		/* 26 */ YY_NOT_ACCEPT,
		/* 27 */ YY_NO_ANCHOR,
		/* 28 */ YY_NOT_ACCEPT,
		/* 29 */ YY_NO_ANCHOR,
		/* 30 */ YY_NOT_ACCEPT,
		/* 31 */ YY_NOT_ACCEPT,
		/* 32 */ YY_NOT_ACCEPT,
		/* 33 */ YY_NOT_ACCEPT,
		/* 34 */ YY_NOT_ACCEPT,
		/* 35 */ YY_NOT_ACCEPT,
		/* 36 */ YY_NOT_ACCEPT,
		/* 37 */ YY_NOT_ACCEPT,
		/* 38 */ YY_NO_ANCHOR,
		/* 39 */ YY_NOT_ACCEPT,
		/* 40 */ YY_NO_ANCHOR,
		/* 41 */ YY_NO_ANCHOR
	};
	private int yy_cmap[] = unpackFromString(1,130,
"29:9,22:2,29:2,0,29:18,21,29:7,2,3,29:2,1,29:2,27,28:10,29:7,12,25,10,20,15" +
",25:2,11,25:2,18,25:2,19,23,13,25,16,17,14,25:6,29:4,24,29,26:4,6,9,26,5,8," +
"26:4,7,26:5,4,26:6,29:5,30:2")[0];

	private int yy_rmap[] = unpackFromString(1,42,
"0,1:3,2,3,4,1:2,5,1,6,1,5,7,1:2,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22," +
"7,23,24,25,26,27,28,29,30,5")[0];

	private int yy_nxt[][] = unpackFromString(31,31,
"-1,1,2,3,4,41:3,18,41,5,19,23,19,25,27,19,29,19:3,6:2,19,7,19,41,20,7:2,8,-" +
"1:35,41,38,41:4,-1:11,17,-1:4,41,-1:8,19:6,-1,21,-1:9,24,-1:4,19,-1:25,6:2," +
"-1:12,41:6,-1:11,17,-1:4,41,-1:8,11:18,-1,11,-1,11:2,-1,11,-1:12,14:11,-1:2" +
",14,-1,14,-1:9,40:6,-1:16,40,-1:8,41:5,9,-1:11,17,-1:4,41,-1:8,19:6,-1:11,2" +
"4,-1:4,19,-1:31,11,-1:15,31,-1:22,41:3,13,41:2,-1:11,17,-1:4,41,-1:8,19:6,-" +
"1:10,10,24,-1:4,19,-1:14,19:11,-1:2,19,-1,19,-1:9,19:6,-1:11,24,-1,26,-1:2," +
"19,-1:28,32,-1:10,19:6,-1:9,28,-1,24,-1:4,19,-1:24,12,-1:14,19:6,-1:8,30,-1" +
":2,24,-1:4,19,-1:19,33,-1:28,39,-1:31,34,-1:26,36,-1:35,37,-1:26,15,-1:35,1" +
"6,-1:18,41:2,22,41:3,-1:11,17,-1:4,41,-1:18,35,-1:20,40:6,-1:11,17,-1:4,40," +
"-1:4");

	public java_cup.runtime.Symbol next_token ()
		throws java.io.IOException {
		int yy_lookahead;
		int yy_anchor = YY_NO_ANCHOR;
		int yy_state = yy_state_dtrans[yy_lexical_state];
		int yy_next_state = YY_NO_STATE;
		int yy_last_accept_state = YY_NO_STATE;
		boolean yy_initial = true;
		int yy_this_accept;

		yy_mark_start();
		yy_this_accept = yy_acpt[yy_state];
		if (YY_NOT_ACCEPT != yy_this_accept) {
			yy_last_accept_state = yy_state;
			yy_mark_end();
		}
		while (true) {
			if (yy_initial && yy_at_bol) yy_lookahead = YY_BOL;
			else yy_lookahead = yy_advance();
			yy_next_state = YY_F;
			yy_next_state = yy_nxt[yy_rmap[yy_state]][yy_cmap[yy_lookahead]];
			if (YY_EOF == yy_lookahead && true == yy_initial) {
				return null;
			}
			if (YY_F != yy_next_state) {
				yy_state = yy_next_state;
				yy_initial = false;
				yy_this_accept = yy_acpt[yy_state];
				if (YY_NOT_ACCEPT != yy_this_accept) {
					yy_last_accept_state = yy_state;
					yy_mark_end();
				}
			}
			else {
				if (YY_NO_STATE == yy_last_accept_state) {
					throw (new Error("Lexical Error: Unmatched Input."));
				}
				else {
					yy_anchor = yy_acpt[yy_last_accept_state];
					if (0 != (YY_END & yy_anchor)) {
						yy_move_end();
					}
					yy_to_mark();
					switch (yy_last_accept_state) {
					case 1:
						{System.out.println("SPL:: COMMA");
             return new Symbol(sym.COMMA); }
					case -2:
						break;
					case 2:
						{System.out.println("SPL:: LP");
             return new Symbol(sym.LP); }
					case -3:
						break;
					case 3:
						{System.out.println("SPL:: RP"); 
             return new Symbol(sym.RP); }
					case -4:
						break;
					case 4:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -5:
						break;
					case 5:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -6:
						break;
					case 6:
						{}
					case -7:
						break;
					case 7:
						{System.out.println("SPL:: Unmatched input "+ yytext());}
					case -8:
						break;
					case 8:
						
					case -9:
						break;
					case 9:
						{System.out.println("SPL:: IF");
	     return new Symbol(sym.IF);}
					case -10:
						break;
					case 10:
						{System.out.println("SPL:: AD"); 
              return new Symbol(sym.AD); }
					case -11:
						break;
					case 11:
						{ }
					case -12:
						break;
					case 12:
						{System.out.println("SPL:: END");
              return new Symbol(sym.END); }
					case -13:
						break;
					case 13:
						{System.out.println("SPL:: THEN");
	      return new Symbol(sym.THEN);}
					case -14:
						break;
					case 14:
						{System.out.println("SPL:: ACTION("+yytext()+")"); 
              return new Symbol(sym.ACTION, yytext()); }
					case -15:
						break;
					case 15:
						{System.out.println("SPL:: SKETCH"); 
              return new Symbol(sym.SKETCH); }
					case -16:
						break;
					case 16:
						{System.out.println("SPL:: CHAPTER"); 
              return new Symbol(sym.CHAPTER); }
					case -17:
						break;
					case 18:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -18:
						break;
					case 19:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -19:
						break;
					case 20:
						{System.out.println("SPL:: Unmatched input "+ yytext());}
					case -20:
						break;
					case 22:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -21:
						break;
					case 23:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -22:
						break;
					case 25:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -23:
						break;
					case 27:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -24:
						break;
					case 29:
						{System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
					case -25:
						break;
					case 38:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -26:
						break;
					case 40:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -27:
						break;
					case 41:
						{System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
					case -28:
						break;
					default:
						yy_error(YY_E_INTERNAL,false);
					case -1:
					}
					yy_initial = true;
					yy_state = yy_state_dtrans[yy_lexical_state];
					yy_next_state = YY_NO_STATE;
					yy_last_accept_state = YY_NO_STATE;
					yy_mark_start();
					yy_this_accept = yy_acpt[yy_state];
					if (YY_NOT_ACCEPT != yy_this_accept) {
						yy_last_accept_state = yy_state;
						yy_mark_end();
					}
				}
			}
		}
	}
}
