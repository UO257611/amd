import java.lang.System;
import java_cup.runtime.Symbol;

%%
%cup
ACTION=("TO_"[A-Z]+)
ACTOR=([A-Z][a-z]*(" "[A-Z][a-z]*)*)
ITEM=([a-z]+(" "[a-z]+)*)
SEPARATOR=((" "|\t|\n)+)
COMMENT=("//"([0-9]|" "|[A-Za-z])*)
%%

","         {System.out.println("SPL:: COMMA");
             return new Symbol(sym.COMMA); }
"("         {System.out.println("SPL:: LP");
             return new Symbol(sym.LP); }
")"         {System.out.println("SPL:: RP"); 
             return new Symbol(sym.RP); }
then	     {System.out.println("SPL:: THEN");
	      return new Symbol(sym.THEN);}

if   	     {System.out.println("SPL:: IF");
	     return new Symbol(sym.IF);}
             
CHAPTER      {System.out.println("SPL:: CHAPTER"); 
              return new Symbol(sym.CHAPTER); }
SKETCH       {System.out.println("SPL:: SKETCH"); 
              return new Symbol(sym.SKETCH); }
END          {System.out.println("SPL:: END");
              return new Symbol(sym.END); }
AD           {System.out.println("SPL:: AD"); 
              return new Symbol(sym.AD); } 

{SEPARATOR} {}

{ACTION}     {System.out.println("SPL:: ACTION("+yytext()+")"); 
              return new Symbol(sym.ACTION, yytext()); }
{ACTOR}      {System.out.println("SPL:: ACTOR("+yytext()+")");
              return new Symbol(sym.ACTOR, yytext()); }
{ITEM}       {System.out.println("SPL:: ITEM("+yytext()+")");
              return new Symbol(sym.ITEM, yytext());  }
{COMMENT}    { }
. 	     		{System.out.println("SPL:: Unmatched input "+ yytext());}
