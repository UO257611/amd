
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Mon Apr 24 12:55:55 CEST 2017
//----------------------------------------------------

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int ACTION = 12;
  public static final int ACTOR = 11;
  public static final int LP = 7;
  public static final int THEN = 10;
  public static final int AD = 3;
  public static final int EOF = 0;
  public static final int CHAPTER = 5;
  public static final int IF = 9;
  public static final int error = 1;
  public static final int COMMA = 6;
  public static final int SKETCH = 2;
  public static final int ITEM = 13;
  public static final int END = 4;
  public static final int RP = 8;
}

