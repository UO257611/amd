import java.lang.System;
class SPL_Scanner {
public static void main(String argv[])
        throws java.io.IOException {
Yylex yy = new Yylex(System.in);
while (yy.yylex() != null){}
}
}

class Yytoken {Yytoken () {}}
%%

SEPARATORS = ((" "|\t|\n)+)
PREP = (ON|IN|INTO|UPON|TO|BY|OVER)
UPPERLET = ([A-Z])
LOWLET = ([a-z])

ACTIONS = ({UPPERLET}{LOWLET}*(ing)|{UPPERLET}{LOWLET}*(ing)" "{PREP})
AD = (AD|ad|Ad|aD)
CHAPTER =  ((C|c)(H|h)(A|a)(P|p)(T|t)(E|e)(R|r))

%%

{ACTIONS}		{System.out.println("SPL:: ACTIONS ("+ yytext()+ " )");}

{CHAPTER}		{System.out.println("SPL:: CHAPTER ("+ yytext()+")");}
{AD}			{System.out.println("SPL:: AD ("+ yytext()+")");}
,			{System.out.println("SPL:: COMMA");}

	

{SEPARATORS}   		{}
. 	     		{System.out.println("SPL:: Input ERROR "+ yytext());}
