import java.lang.System;
import java_cup.runtime.Symbol;

%%

%cup
DIGIT=([0-9])
INTEGER=({DIGIT}+)
BLANK = (" "|\t)
REAL = ({INTEGER}"."|{INTEGER}"."{INTEGER}|"."{INTEGER})
COMMENT = ((//)({DIGIT}|{LETTER}|{BLANK})*))
LETTER= ([a-zA-Z])
EXT= ({LETTER}|{DIGIT})
IDENT = ({LETTER}{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?{EXT}?)

%%
begin			{System.out.println("SCANNER:: BEGIN");
                         return new Symbol(sym.BEGIN);}
end			{System.out.println("SCANNER:: END");
                         return new Symbol(sym.END);}
print			{System.out.println("SCANNER:: PRINT");
                         return new Symbol(sym.PRINT);}
;			{System.out.println("SCANNER:: EOS");
                         return new Symbol(sym.EOS);}
{IDENT}			{System.out.println("SCANNER:: IDENT <"+ yytext() +">");
			 return new Symbol(sym.IDENT,new String(yytext()));}
{REAL}			{System.out.println("SCANNER:: NUMBER <"+ yytext() +">");
 			 return new Symbol(sym.NUMBER,new Double(yytext()));}
{COMMENT}		{System.out.println("SCANNER:: COMMENT");}
"("			{System.out.println("SCANNER:: LP");
                         return new Symbol(sym.LP);}
")"			{System.out.println("SCANNER:: RP");
                         return new Symbol(sym.RP	);}
"+"      	     	{System.out.println("SCANNER:: PLUS");
			 return new Symbol(sym.PLUS);}
"*"      	     	{System.out.println("SCANNER:: MULT");
			 return new Symbol(sym.MULT);}
-       	     	{System.out.println("SCANNER:: MINUS");
			 return new Symbol(sym.MINUS);}
/       	     	{System.out.println("SCANNER:: DIV");
			 return new Symbol(sym.DIV);}
"<"       	     	{System.out.println("SCANNER:: LT");
			 return new Symbol(sym.LT);}
>       	     	{System.out.println("SCANNER:: GT");
			 return new Symbol(sym.GT);}
==       	     	{System.out.println("SCANNER:: EQ");
			 return new Symbol(sym.EQ);}
=       	     	{System.out.println("SCANNER:: SET");
			 return new Symbol(sym.SET);}
{INTEGER}    		{System.out.println("SCANNER:: NUMBER <"+yytext()+">");
			 return new Symbol(sym.NUMBER,new Double(yytext()));}

(" "|\t|\n)+   		{}
. 	     			{System.out.println("SCANNER:: Unmatched input "+ yytext());}
